import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Router } from '@angular/router';

export class Pokemon {
  name:string;
  url:string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(CdkVirtualScrollViewport, {static: false})
  public viewport: CdkVirtualScrollViewport;

  public nextRouter: String;
  public pokemonArray: Array<Pokemon>;

  constructor(private service: PokemonService, private router: Router,) {}

  ngOnInit(): void {
    this.loadPokemons();
  }

  showPokemon(pokemon) {
    this.router.navigate([`/detalhes`], {
      state: {
        data: { urlPokemon: pokemon.url}
      }
    })
    
  }

  loadPokemons() {
    this.service.getPokemons().subscribe(response => {
      console.log(response);
      this.pokemonArray = response.results;
      this.nextRouter = response.next;
    }, (error: HttpErrorResponse) => {
      
    });
  }

  getNewData() {
    let endData = this.viewport.getRenderedRange().end;
    if (this.pokemonArray && endData === this.pokemonArray.length) {
      this.service.getNextRouter(this.nextRouter).subscribe(response => {
        this.nextRouter = response.next;
        this.pokemonArray = Array.from(new Set(this.pokemonArray.concat(response.results)));
      }, (error: HttpErrorResponse) => {
        
      });
    }
  }

}
