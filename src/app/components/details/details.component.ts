import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public url;
  public pokemon;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PokemonService
    ) {
    this.route.params.subscribe((params) => {
      if (!this.router.getCurrentNavigation().extras.state) {
        this.back();
      }
      this.url = this.router.getCurrentNavigation().extras.state.data.urlPokemon;
    });
   }

  ngOnInit(): void {
    this.service.getPokemon(this.url).subscribe(response => {
      this.pokemon = response;
    }, (error: HttpErrorResponse) => {

    })
    
  }

  back() {
    this.router.navigate(['']);
  }

}
