import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private apiServerUrl = environment.urlToApi;
  private limitPage = environment.limitPage;

  constructor(private http: HttpClient) { }

  public getPokemons(): Observable<any>{
    return this.http.get(`${this.apiServerUrl}/pokemon/?limit=${this.limitPage}`);
  }

  public getNextRouter(router): Observable<any>{
    return this.http.get(router);
  }

  public getPokemon(router): Observable<any>{
    return this.http.get(router);
  }
  
}
